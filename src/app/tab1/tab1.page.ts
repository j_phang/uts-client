import { Component, OnInit } from '@angular/core';
import { FilmService } from '../film.service';
import { Response } from '@angular/http';
import { NavController, ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
const KEY_DATA_LOCAL = "dataLocal";

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
  filmList: any;
  id: any = null;
  listLocal: any;
  constructor(
    public modalController: ModalController,
    public filmService: FilmService,
    public navCtrl: NavController,
    private router: Router,
    public storage: Storage
  ){}

  ngOnInit(){
    this.loadFilm();
    this.storage.set('name', 'mn');
    this.storage.get('name').then((val) => {
      console.log('Your Name Is ', val);
    })
  }

  loadFilm(){
    this.filmService.loadFilm().subscribe((response: Response)=> {
      let data = response.json();
      this.filmList = data;
    });
  }

  ambilDataLocal(){
    this.storage.get(KEY_DATA_LOCAL).then((data) => {
      if(data != null){
        this.listLocal = JSON.parse(data);
        console.log(this.listLocal);
      } else {
        this.listLocal =[];
        console.log('empty');
      }
    })
  }

  goDetail(data){
    var id = data;
    localStorage.setItem('KEY_DATA_LOCAL', id);
    var wow = localStorage.getItem('KEY_DATA_LOCAL');
    console.log(wow);
    this.router.navigate(['/details']);
  }
}
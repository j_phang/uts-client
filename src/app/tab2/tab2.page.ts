import { Component } from '@angular/core';
import { NavController, ModalController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { AdminPage } from '../admin/admin.page';
import { FilmService } from '../film.service';
import { Response } from '@angular/http';
@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  admin: any;
  admin1 = {
    username: '',
    password: ''
  };
  filmList: any;
  constructor(
    public navCtrl: NavController,
    public modalController: ModalController,
    public filmService: FilmService
    ){}

  async masuk(){
    const modal = await this.modalController.create({
      component: AdminPage
    });
    modal.onDidDismiss().then(() => {});
    return await modal.present();
  }

  async login(){
    this.filmService.getAdmin(this.admin1).subscribe((response: Response) => {
    if(response){
      this.admin = response.json();
      if (this.admin[0].username == this.admin1.username){
        if (this.admin[0].password == this.admin1.password){
          this.filmService.message('Berhasil Login !');
          this.masuk();
        } else {
          this.filmService.message('Username atau Password Salah !');  
        }
      } else {
        this.filmService.message('Username atau Password Salah !');
      }
    } else {
      this.filmService.message('Username atau Password Salah !');
    }
    });
  }
}
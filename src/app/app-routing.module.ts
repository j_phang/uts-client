import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'login', canActivate: [], loadChildren: './tab2/tab2.module'},
  { path: 'admin', loadChildren: './admin/admin.module#AdminPageModule' },
  { path: 'admin-add', loadChildren: './admin-add/admin-add.module#AdminAddPageModule' },
  { path: 'admin-details', loadChildren: './admin-details/admin-details.module#AdminDetailsPageModule' },
  { path: 'details', loadChildren: './details/details.module#DetailsPageModule' }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}

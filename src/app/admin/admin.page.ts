import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FilmService } from '../film.service';
import { ModalController } from '@ionic/angular';
import { Response } from '@angular/http';
import { AdminAddPage } from '../admin-add/admin-add.page';
import { AdminDetailsPage } from '../admin-details/admin-details.page';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.page.html',
  styleUrls: ['./admin.page.scss'],
})
export class AdminPage{
  filmList: any;
  constructor(
    private router: Router,
    public filmService: FilmService,
    public modalController: ModalController
  ) {
    this.loadFilm();
  }

  logout(){
    console.log("Berhasil Logout");
    this.modalController.dismiss();
    this.filmService.message('Logout berhasil');
  }

  loadFilm(){
    this.filmService.loadFilm().subscribe((response: Response)=> {
      this.filmList = response.json();
      console.log(this.filmList);
    });
  }

  async goAddFilm(){
    const modal = await this.modalController.create({
      component: AdminAddPage
    });
    modal.onDidDismiss().then(() => { this.loadFilm() });
    return await modal.present();
  }

  async goDetails(film){
    const modal = await this.modalController.create({
      component: AdminDetailsPage,
      componentProps: { value: film }
    });
    modal.onDidDismiss().then(() => { this.loadFilm() });
    return await modal.present();
  }

  deleteFilm(id){
    this.filmService.deleteFilm(id).subscribe((response: Response) => {
      if(response.ok){
        this.filmService.message('BERSAHIL HAPUS');
      }
      this.loadFilm();
    })
  }
}

import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class FilmService {

  constructor(public http: Http, public toastController: ToastController) { }
  
  loadFilm(){
    return this.http.get('http://localhost:8081/api/film');
  }
  addFilm(data){
    return this.http.post('http://localhost:8081/api/filming', data);
  }
  updateFilm(data){
    return this.http.post('http://localhost:8081/api/filming/' + data.id, data);
  }
  deleteFilm(id){
    console.log(id);
    return this.http.delete('http://localhost:8081/api/filming/' + id);
  }
  getFilm(id){
    return this.http.get('http://localhost:8081/api/filmid/' + id);
  }
  getAdmin(data){
    return this.http.get('http://localhost:8081/api/admin/' + data.username + '/'+ data.password, data);
  }
  async message(msg){
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
  });
  toast.present();
  }
}
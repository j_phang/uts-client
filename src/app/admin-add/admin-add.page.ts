import { Component, OnInit } from '@angular/core';
import { FilmService } from '../film.service';
import { ModalController } from '@ionic/angular';
import { Response } from '@angular/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-add',
  templateUrl: './admin-add.page.html',
  styleUrls: ['./admin-add.page.scss'],
})
export class AdminAddPage implements OnInit {
  data = {
    id: 'NULL',
    judul: '',
    images: '',
    durasi: '',
    tahun: '',
    sutradara: ''
  };
  constructor(
    public filmService: FilmService,
    public modalController: ModalController,
    public router: Router
  ) { }

  ngOnInit() {
  }

  addFilm(){
    console.log(this.data);
    this.filmService.addFilm(this.data).subscribe((response: Response) => {
      if(response){
        this.filmService.message('DATA BERHASIL DITAMBAH');
      } else {
        this.filmService.message('ADA YANG SALAH');
      }
      this.modalController.dismiss();
    });
  }

  back(){
    this.modalController.dismiss();
  }
}

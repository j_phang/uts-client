import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { FilmService } from '../film.service';
import { Response } from '@angular/http';

@Component({
  selector: 'app-admin-details',
  templateUrl: './admin-details.page.html',
  styleUrls: ['./admin-details.page.scss'],
})
export class AdminDetailsPage implements OnInit {
  film: any;
  data = {
    id: 'NULL',
    judul: '',
    images: '',
    durasi: '',
    tahun: '',
    sutradara: ''
  };
  constructor(
    public navParams: NavParams,
    public filmService: FilmService,
    public modalController: ModalController
  ) {
    this.film = this.navParams.data.value;
    console.log(this.film);
    this.data.id = this.film.id;
    this.data.judul = this.film.judul;
    this.data.images = this.film.images;
    this.data.durasi = this.film.durasi;
    this.data.tahun = this.film.tahun;
    this.data.sutradara = this.film.sutradara;
  }

  ngOnInit() {
  }

  updateFilm(){
    console.log(this.data);
    this.filmService.updateFilm(this.data).subscribe((response: Response) => {
      if(response){
        this.filmService.message('DATA DIUBAH');
      } else {
        this.filmService.message('ADA YANG SALAH');
      }
      this.modalController.dismiss();
    })
  }

  back(){
    this.modalController.dismiss();
  }
}

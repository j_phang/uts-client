import { Component, OnInit } from '@angular/core';
import { ModalController, NavController, NavParams } from '@ionic/angular';
import { FilmService } from '../film.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {
  filmDetail: any;
  constructor(
    public filmService: FilmService,
    public modalController: ModalController,
    public router: Router,
    public route: ActivatedRoute
  ) {
    
  }

  ngOnInit() {
    var id = localStorage.getItem('KEY_DATA_LOCAL');
    this.filmService.getFilm(id).subscribe((response: Response)=> {
      let data = response.json();
      this.filmDetail = data;
    });
    console.log(this.filmDetail);
  }

  back(){
    localStorage.removeItem('KEY_DATA_LOCAL');
    this.router.navigate(['/tabs']);
  }
}

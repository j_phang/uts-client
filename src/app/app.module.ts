import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpModule } from '@angular/http';

import { FormsModule } from '@angular/forms';

import { IonicStorageModule } from '@ionic/storage';
import { AdminPage } from './admin/admin.page';
import { AdminPageModule } from './admin/admin.module';
import { AdminAddPipe } from './admin-add.pipe';
import { AdminAddPage } from './admin-add/admin-add.page';
import { AdminDetailsPage } from './admin-details/admin-details.page';
import { DetailsPageModule } from './details/details.module';

@NgModule({
  declarations: [AppComponent, AdminAddPipe, AdminAddPage, AdminDetailsPage],
  entryComponents: [AdminPage, AdminAddPage, AdminDetailsPage],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, HttpModule, FormsModule, IonicStorageModule.forRoot(), AdminPageModule, DetailsPageModule, IonicStorageModule.forRoot()],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
